from setuptools import find_packages, setup

setup(
    name="test-cli-tool",
    version="1",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "test-cli-tool = test_cli_tool.cli:main",
            "test-cli-tool-two = test_cli_tool.cli:two"
		],
	},
)
